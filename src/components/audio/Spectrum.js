import {reduceArray} from '../../utils.js'

export class Spectrum {
  constructor(spectrum) {
    this.spectrum = spectrum
  }

  trim(offset){
    this.spectrum = this.spectrum.slice(0, this.spectrum.length + offset) 
    return this
  }

  simplify(threshold = 10){
    this.spectrum = reduceArray(this.spectrum, threshold)
    return this
  }

  asArray(){
    return this.spectrum
  }
}