import { Spectrum } from "./Spectrum.js";
import { CONFIG } from "../../config.js";

const defaultConfig = {
  numberOfBars: 50
};

class Bar {
  constructor(x, y, width, height, color) {
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
    this.color = color;
  }

  draw() {
    // noStroke();
    noFill()
    stroke(255)
    colorMode(HSB);
    // fill(this.color, 100, 100);
    rect(this.x, this.y, this.width, this.height);
  }
}

export class BarsEqualizerHorizontal {
  constructor(config = defaultConfig) {
    this.config = { ...defaultConfig, ...config };
  }

  draw(fullSpectrum) {
    const spectrum = new Spectrum(fullSpectrum)
      .trim(-300)
      .simplify(this.config.numberOfBars)
      .asArray();

    const numberOfBars = spectrum.length;

    spectrum.forEach((frequency, index) => {
      const barHeight = -height + map(frequency, 0, 100, CONFIG.height, 200);
      const barWidth = width / numberOfBars;
      const xCenter = map(index, 0, numberOfBars, 0, CONFIG.width);
      const xPos = xCenter + barWidth / 2
      const yPos = height + barHeight;
      const hue = 360/numberOfBars * index

      new Bar(xPos, yPos, barWidth, barHeight, hue).draw();
      new Bar(xPos + 10, yPos + 10, barWidth, barHeight, hue).draw();
      new Bar(xPos - 140, yPos + 800, barWidth, barHeight, hue).draw();
      new Bar(xPos + 340, yPos + 150, barWidth, barHeight, hue).draw();
    });
  }
}
