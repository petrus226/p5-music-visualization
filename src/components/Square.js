import { CONFIG } from "../config.js";

export class Square {
  constructor(maxSize = 100) {
    this.maxSize = maxSize;
  }

  draw(magnitude) {
    const size = this.maxSize * magnitude;
    const xPos = CONFIG.width / 2;
    const yPos = CONFIG.height / 2;

    strokeWeight(1);
    rectMode(CENTER);
    stroke(0, 255, 0);
    translate(0, 0);
    noFill();
    rect(xPos, yPos, size, size);
  }
}
