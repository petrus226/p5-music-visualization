import { BarsEqualizerHorizontal } from "./components/audio/BarsEqualizerHorizontal.js";
import { BarsEqualizerVertical } from "./components/audio/BarsEqualizerVertical.js";
import { ImageAmplitudeMeter } from "./components/audio/ImageAmplitudeMeter.js";
import { CONFIG } from "./config.js";

const equalizer = new BarsEqualizerHorizontal({
  numberOfBars: 10
});
const equalizerVertical = new BarsEqualizerVertical({
  numberOfBars: 10
});
const amplitude = new p5.Amplitude();
const mic = new p5.AudioIn();
const fft = new p5.FFT();
let faceBass;
let facehighMid;
let faceMid;
let color = 0;
let bassAmplitude;
let highMidAmplitude;
let midAmplitude;
let maxBassAmplitude = 0;

const windowResized = () => {
  resizeCanvas(CONFIG.width, CONFIG.height);
};

const mouseClicked = e => {
  const $audioNotice = document.querySelector("#audio-notice").remove();
  // This is mandatory to work under browsers privacy setup
  // isAudioContextLoaded = true
  userStartAudio().then(() => {
    console.log("audio context started");
    if ($audioNotice) {
      $audioNotice.remove();
    }
  });
};

const preload = () => {
  faceBass = new ImageAmplitudeMeter(
    "public/img/lluis.png",
    CONFIG.center.x,
    CONFIG.center.y,
    CONFIG.height
  );
  facehighMid = new ImageAmplitudeMeter(
    "public/img/vilva.png",
    CONFIG.center.x / 2,
    CONFIG.center.y / 2,
    CONFIG.height / 2
  );
  faceMid = new ImageAmplitudeMeter(
    "public/img/jaume.png",
    CONFIG.center.x + 500,
    CONFIG.center.y + 300,
    CONFIG.height / 3
  );
};

const setup = () => {
  createCanvas(CONFIG.width, CONFIG.height);
  mic.start();
  amplitude.setInput(mic);
  fft.setInput(mic);
};

setInterval(() => {

  if(bassAmplitude > maxBassAmplitude){
    maxBassAmplitude = bassAmplitude
  }

  if (bassAmplitude > maxBassAmplitude * 0.7) {
    console.log("Bombaco!");
    color = Math.floor((Math.random() * 255));
  }
}, 500)

const draw = () => {
  bassAmplitude = fft.getEnergy("bass") / 255;
  highMidAmplitude = fft.getEnergy("highMid") / 255;
  midAmplitude = fft.getEnergy("mid") / 255;
  colorMode(HSB)
  background(color, 100, 40);

  let spectrum = fft.analyze();
  const volume = mic.getLevel();
  equalizer.draw(spectrum);
  equalizerVertical.draw(spectrum);
  faceBass.draw(bassAmplitude);
  facehighMid.draw(highMidAmplitude);
  faceMid.draw(midAmplitude);
};

window.preload = preload;
window.setup = setup;
window.draw = draw;
window.mouseClicked = mouseClicked;
window.windowResized = windowResized;
